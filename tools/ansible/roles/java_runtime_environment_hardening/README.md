# Ansible Role: java_runtime_environment_hardening

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
[![Ansible](https://img.shields.io/badge/ansible-%231A1918.svg?style=for-the-badge&logo=ansible&logoColor=white)](https://www.ansible.com/)

[[_TOC_]]

**Note:** This Ansible role contains a part that checks the hardening settings.
The checks are performed using Chef&reg;'s tool
[InSpec &reg;](https://www.chef.io/products/chef-inspec) and the checks
themselves are created by &copy; 2018-2020 The MITRE Corporation and were
originally desigend for JRE8. The InSpec&reg; profiles can be found
[here](https://github.com/mitre/oracle-java-runtime-environment-8-unix-stig-baseline).

## Description

This Ansible role configures hardening settings for the Java Runtime
Environment (JRE). It does so by placing the configuration files
`deployment.config` and `deployment.properties` in the configuration directory
of the JRE. In case of further interest on the settings that are chosen, c.f.
the template of the
[deployment.porperties](./templates/deployment.properties.j2).

## Requirements

Supported versions:

* Linux Distribution
  * Debian Family
    * Debian
      * Bullseye (11)
      * Bookworm (12)

**Note:** Other versions are likely to work but have not been tested.

## Role Variables

There are no variables to be set in this role.

## Dependencies

This role requires that Java is preinstalled on the system, since it just
places the configuration and checks it.

## Example Playbook

A possible playbook could look like follows:

```yaml
- name: JRE hardening
  hosts: java_hosts
  become: true
  become_method: ansible.builtin.sudo
  roles:
    - java_runtime_environment_hardening
```

# License
Apache 2.0

# Authors and Contributors

[André Breuer](mailto://andre.breuer@ndaal.eu) / ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne <br/>
[Pierre Gronau](mailto://pierre.gronau@ndaal.eu) / ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne <br/>